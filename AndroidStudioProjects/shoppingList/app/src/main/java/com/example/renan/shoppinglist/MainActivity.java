package com.example.renan.shoppinglist;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends  Activity {
    public static final int CAMERA_REQUEST_CODE = 0;
    public static final int callPermissionRequest = 1;
    public static final int SPEECH_RECOGNITION_REQUEST = 2;
    private static final int UPDATE_PHONE_NUMBER = 3;
    ImageButton prodcut_Img_btn;

    EditText productName_Et;
    private TextView totalPriceNum;

    private ArrayList<Product> productArrayList = ProductsList.getInstance().getList();
    private ProductAdapter productlistAdapter;
    public Bitmap bitmap;
    public ProgressBar progressBar;
    private String phoneNumber = "0528800676";
    private boolean firstRun = true;
    private ImageButton searchBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Dialog thisDialog = new Dialog(MainActivity.this);

        TextView productNameTv = findViewById(R.id.name_product);
        TextView amountProductTv = findViewById(R.id.amount_product);
        TextView priceProductTv = findViewById(R.id.Price_Product);
        totalPriceNum = findViewById(R.id.total_price_num);
        ListView listProduct = findViewById(R.id.product_list);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setProgress(0);

        productlistAdapter = new ProductAdapter(this, productArrayList, progressBar);
        listProduct.setAdapter(productlistAdapter);

        ImageButton settingBtn = findViewById(R.id.setting_btn);
        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                intent.putExtra("phone_number", phoneNumber);
                startActivityForResult(intent,UPDATE_PHONE_NUMBER);
            }
        });

        ImageButton sendMsgbtn = findViewById(R.id.msg_btn);
        sendMsgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent .setData(Uri.parse("sms:" + phoneNumber));
                startActivity(intent);
            }
        });

        ImageButton addProductBtn = findViewById(R.id.add_new_product);
        addProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thisDialog.setContentView(R.layout.layout_dialog);
                thisDialog.setTitle(R.string.add_new_product);
                thisDialog.show();

                Button cancel_Btn = thisDialog.findViewById(R.id.cancel_btn_d);
                cancel_Btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        thisDialog.cancel();
                    }
                });
                productName_Et = thisDialog.findViewById(R.id.name_product_d);
                Button ok_Btn = thisDialog.findViewById(R.id.ok_btn_d);
                ok_Btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText amountProduct_Et = thisDialog.findViewById(R.id.amount_product_d);
                        EditText Price_Product_Et = thisDialog.findViewById(R.id.Price_Product_d);

                        String name = productName_Et.getText().toString();
                        String amount = (amountProduct_Et.getText().toString());
                        String Price = (Price_Product_Et.getText().toString());

                        Product newProductRow = new Product(name, amount, Price, false, bitmap);
                        productArrayList.add(newProductRow);
                        productlistAdapter.updateProgressBar();
                        productlistAdapter.notifyDataSetChanged();
                        thisDialog.dismiss();
                        Toast.makeText(MainActivity.this, R.string.new_row, Toast.LENGTH_SHORT).show();
                        totalPriceNum.setText(String.valueOf(ProductsList.getInstance().calcTotalPrice()));
                        progressBar.setProgress(ProductsList.getInstance().updateProgressBar());
                        bitmap = null;
                    }
                });

                ImageButton micBtn = thisDialog.findViewById(R.id.mic_btn_d);
                micBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, R.string.speek);
                        startActivityForResult(intent, SPEECH_RECOGNITION_REQUEST);
                    }
                });

                prodcut_Img_btn = thisDialog.findViewById(R.id.image_btn_d);
                prodcut_Img_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, CAMERA_REQUEST_CODE);
                    }
                });

                searchBtn = thisDialog.findViewById(R.id.search_btn_d);
                searchBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                        intent.putExtra(SearchManager.QUERY,productName_Et.getText().toString());
                        startActivity(intent);
                    }
                });
            }
        });

        ImageButton callBtn = findViewById(R.id.call_btn);
        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT < 23) {
                    makeCall();
                } else {
                    int hasCallPermision = checkSelfPermission(Manifest.permission.CALL_PHONE);
                    if (hasCallPermision == PackageManager.PERMISSION_GRANTED) {
                        makeCall();
                    } else {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, callPermissionRequest);
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        productlistAdapter.notifyDataSetChanged();
        totalPriceNum.setText(String.valueOf(ProductsList.getInstance().calcTotalPrice()));
        progressBar.setProgress(ProductsList.getInstance().updateProgressBar());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == callPermissionRequest) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeCall();
            } else {
                Toast.makeText(this, R.string.premission, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void makeCall() {

        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                if (data != null && data.getExtras() != null) {
                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    this.bitmap = bitmap;
                    prodcut_Img_btn.setImageBitmap(bitmap);
                }
                break;

            case SPEECH_RECOGNITION_REQUEST:
                if (resultCode == RESULT_OK) {
                    ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String result = results.get(0);
                    if (result == null || result.length() == 0) {
                        Toast.makeText(this, R.string.speak_slowly, Toast.LENGTH_SHORT).show();
                    } else {
                        productName_Et.setText(result);
                    }
                }
            case UPDATE_PHONE_NUMBER: {
                if (resultCode == RESULT_OK) {
                    phoneNumber = data.getStringExtra("updated_phone_number");
                }
                break;

            }


        }
    }
}
