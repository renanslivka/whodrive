package com.homedrill.renan.sportnews;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.homedrill.renan.sportnews.models.Article;

import java.util.ArrayList;
import java.util.List;

import static com.homedrill.renan.sportnews.Adapter.getArticles;

public class MainActivity extends AppCompatActivity /*implements SwipeRefreshLayout.OnRefreshListener*/{
    public static final String API_KEY = "781fe53ce8f74fc48f3e57d70db1088b";
    private RecyclerView.LayoutManager layoutManager;
    private List<Article> articles = new ArrayList<>();
    private Adapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    private NotificationManagerCompat NotificationManager;
    private AlarmManager alarmManager;
    private PendingIntent actionIntent;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.AddFrgment(new FragmentGeneralNews(this),"General");//Add fragment General
        viewPagerAdapter.AddFrgment(new FragmentGeneric(articles,this,"technology"),"Technology");//Add fragment
        viewPagerAdapter.AddFrgment(new FragmentGeneric(articles,this,"sports"),"Sport");//Add fragment
        viewPagerAdapter.AddFrgment(new FragmentGeneric(articles,this,"business"),"Business");//Add fragment
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);


        NotificationManager = NotificationManagerCompat.from(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.update_every_1_minute:
                pushNotification(60);
                break;

            case R.id.update_every_5_minute:
                pushNotification(300);
                break;

            case R.id.update_every_10_minute:
                pushNotification(600);
                break;

            case R.id.cancel_refresh:
                CancelNotification();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void pushNotification(final int seconds)
    {
        Intent broadcastIntent = new Intent(getApplicationContext(),RefreshDataReceiver.class);
        broadcastIntent.putExtra("seconds",seconds);
        broadcastIntent.putExtra("url", getArticles().get(0).getUrl());
        broadcastIntent.putExtra("title", getArticles().get(0).getTitle());
        broadcastIntent.putExtra("img", getArticles().get(0).getUrlToImage());
        broadcastIntent.putExtra("date",getArticles().get(0).getPublishedAt());
        broadcastIntent.putExtra("source", getArticles().get(0).getSource().getName());
        broadcastIntent.putExtra("author", getArticles().get(0).getAuthor());

        actionIntent = PendingIntent.getBroadcast(getApplicationContext(),
                0,broadcastIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis() + 2000,
                seconds*1000, actionIntent);
    }

    private void CancelNotification(){
//Cancel the alarm
        Intent intent = new Intent(this, RefreshDataReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}
